/**  Copyright (C) 2013  Authors, TALSOFT S.R.L.
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 
 TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
 See the site of Enigma-gpg: http://www.enigmagpg.com
	
*/

exports.getKeyUsers = getKeyUsers;

var controller = require("AppController.js");

/**
* Param Comment
*
* @type {string}
* @private
*/

var getKeyUsers_ = 'https://app.enigmagpg.com/api/v1/keycontainer/?format=json';

var localStorage = require("dataStorage.js");
var Request = require("sdk/request").Request;

function getKeyUsers(){
	getKeyUsersRequest_(localStorage.getToken());
}


/**
* Function Comment
* 
*
* @param {String} An access token.
* @return {string} .
* @private
*/
function getKeyUsersRequest_(accessToken) {
	var request = Request({	url: getKeyUsers_,
				onComplete: parseKeyUserRequest_, 
				headers: {"Authorization": "OAuth "+accessToken}
			  });
	request.get();	
	
/*	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onload = this.parseKeyUserRequest_.bind(this);
	xmlhttp.open("GET",this.getKeyUsers_,true);
	xmlhttp.setRequestHeader("Authorization","OAuth "+accessToken);
	xmlhttp.send();
	*/
};

/**
* Function Comment
* 
*
* @param {String} An access token.
* @param {ProgressEvent} e The XHR ProgressEvent.
* @private
*/
function parseKeyUserRequest_(request){
	if (request.status == 200){
		console.log(request.text);
		controller.users(request.json);
	}else{
		controller.usersNotFound();
	}
};
