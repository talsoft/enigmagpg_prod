/**  Copyright (C) 2013  Authors, TALSOFT S.R.L.
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 
 TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
 See the site of Enigma-gpg: http://www.enigmagpg.com
	
*/

/* LOGIN */
exports.start = start;
exports.loginsuccess = loginsuccess;
exports.loginerror = loginerror;
/* KEY USERS */
exports.users = users;
exports.usersNotFound = usersNotFound;
/* MENU HANDLER */
exports.menuHandler = menuHandler;

/* IMPORTS */
var panel;
var data = require("sdk/self").data;
var login = require("loginOauth.js");
var keyusers = require("keyusers.js");
var selection = require("sdk/selection");

function start(){
	panel = require("sdk/panel").Panel({
	  width: 650,
	  height: 500,
	  contentURL: data.url("plugin.html"),
	  contentScriptFile: [data.url("js/jquery.js"), data.url("js/controller.js"), data.url("js/keyusers.js"), data.url("js/gpg.js"), data.url("js/openpgp.js"), data.url("js/bootstrap.min.js"), data.url("js/aes2.js"), data.url("js/base64.js")]
	});
	 
	// Create a widget, and attach the panel to it, so the panel is
	// shown when the user clicks the widget.
	require("sdk/widget").Widget({
	  label: "EnigmaGPG",
	  id: "enigma",
	  contentURL: data.url("images/icon.png"),
	  panel: panel
	});

	//Escuchamos los posibles eventos informados por la GUI
	/*********************** LOGIN ***********************/
	panel.port.on("login", function(params){
		login.login(params["username"],params["password"]);
	});

	panel.port.on("logout", function(){
		login.logout();
	});

	/*********************** KEYUSERS ***********************/
	panel.port.on("getUsers", function(){
		keyusers.getKeyUsers();
	});
	
	/*********************** OTHERS ***********************/
	panel.port.on("openTab", function(params){
		//open a new tab with a dummy webpage
		require("sdk/tabs").open({							
			url: params["url"] 
		});
	});

	panel.port.on("copyToClipboard", function(value){
		require("sdk/clipboard").set(value);
	});

	
	
	//End up checking if stored token is valid, if it is one.
	login.checkSession();
};

/*********************** LOGIN ***********************/
function loginsuccess(userData){
	panel.port.emit("loginsuccess",userData);
};

function loginerror(error){
	panel.port.emit("loginerror", error);
}

/*********************** KEYUSERS ***********************/
function users(u){
	panel.port.emit("users",u);
}

function usersNotFound(){
	panel.port.emit("usersNotFound");
}

/*********************** MENU HANDLER ***********************/
function menuHandler(menuData){
	var selectedText = selection.text;
	switch(menuData){
		case "cipher":
			if(selectedText != "")
				panel.port.emit("setCipherText", selectedText);
				panel.show();
			break;
		case "decipher":
			if(selectedText != "")
				panel.port.emit("setDecipherText", selectedText);
				panel.show();
			break;
	}
};
