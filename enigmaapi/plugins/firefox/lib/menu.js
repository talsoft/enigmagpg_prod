/**  Copyright (C) 2013  Authors, TALSOFT S.R.L.
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 
 TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
 See the site of Enigma-gpg: http://www.enigmagpg.com
	
*/
exports.buildMenu = buildMenu;


// Public function that creates the context menu that is displayed anywhere and returns it
function buildMenu(){

	//Define the variable context menu
	var contextMenu = require("sdk/context-menu");
	
	/********************** Items Definition  ***************************/
/*	var signin = contextMenu.Item({
		label: "Sign in",
		data: "signin"
	});
*/
	var cipher = contextMenu.Item({
		label: "Cipher",
		data: "cipher"
	});

	var decipher = contextMenu.Item({
		label: "Decipher",
		data: "decipher"
	});

	/*  If context is not being used and in the context script there is a listener to the 'context' event,
		when the function associated with that listener returns true, the menu is shown. It only works
		for main menus (in our case the menu 'Enigma GPG'
	*/
	
	/********************** Main Menu Definition **********************/
	var enigmagpgMenu = contextMenu.Menu({
		label: "EnigmaGPG",		
		/* End test*/
		contentScript: 'self.on("context",function (node){return true;}); ' +
					   'self.on("click",function(node,data){self.postMessage(data);})',
		onMessage: require("AppController.js").menuHandler, //asociate handleMenu function

		items: [cipher, decipher],
		image: require("sdk/self").data.url("images/icon.png")
	});

	return enigmagpgMenu;
}




