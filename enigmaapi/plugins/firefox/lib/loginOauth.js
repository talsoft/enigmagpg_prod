/**  Copyright (C) 2013  Authors, TALSOFT S.R.L.
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 
 TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
 See the site of Enigma-gpg: http://www.enigmagpg.com
	
*/

exports.login 			= login;
exports.logout			= logout;
exports.checkSession		= checkSession;

var CLIENT_ID = "c0d5c00fd9206e3e72a2";
var CLIENT_SECRET = "fb574ed852fe1d5bf1a4ec0490a5e0bf581ac3c1";
var SCOPE = "write";
var GRANT_TYPE ="password";

var userData;

var localStorage = require("dataStorage.js");
var Request = require("sdk/request").Request;

var controller = require("AppController.js");

  /**
   * Param Comment
   *
   * @type {string}
   * @private
   */
  var getAccessToken_ = 'https://app.enigmagpg.com/oauth2/access_token';

  /**
   * Param Comment
   *
   * @type {string}
   * @private
   */
var   getUserData_ = 'https://app.enigmagpg.com/api/v1/user/?format=json';

  /**
   * Param Comment
   *
   * @type {string}
   * @private
   */
var   username_ = '';

  /**
   * Param Comment
   *
   * @type {string}
   * @private
   */ 
var  password_ = '';



  /**
   * Function Comment
   * 
   *
   * @public
   */
  function login(username,password){
    username_ = username;
    password_ = password;
    requestAccessToken_();
  };
  
  /**
   * Function Comment
   * 
   *
   * @public
   */
  function logout(){
    // Delete user info
    userData = null;
    // Delete access token
    localStorage.deleteToken();    
  };


  /**
   * Function Comment
   * 
   *
   * @public
   */
    function checkSession(){
    // Check if we have a previous access token
    if(localStorage.isTokenDefined()){
	return getPGPKeys_(localStorage.getToken());  
    }
    controller.loginerror();
  };


  /**
   * Function Comment
   * 
   *
   * @private
   */
   function requestAccessToken_() {    
	var params = {	
		'client_id': CLIENT_ID,
		'client_secret': CLIENT_SECRET,
		'grant_type': GRANT_TYPE, 
		'username': username_ ,
		'password': password_
	}	
	var request = Request({	url: getAccessToken_,
				onComplete: parseAccesTokenResponde_, 
				content: params,
				headers: {"Content-type": "application/x-www-form-urlencoded"}
			  });
	request.post();
  };

  /**
   * Function Comment
   * 
   * 
   *
   * @param {ProgressEvent} e The XHR ProgressEvent.
   * @private
   */
  function parseAccesTokenResponde_(response) {
	switch (response.status){
		case 200:
			var jsonObj = response.json;
			if(jsonObj.hasOwnProperty('access_token')){
				// We get access token successfully
				getPGPKeys_(jsonObj.access_token);
				// Storage acces token using local storage
				localStorage.setToken(jsonObj.access_token);
			}else if(jsonObj.hasOwnProperty('error')){
				var error = {"error": jsonObj.error};
				controller.loginerror(error);				
			}
			break;
		default:
			var error = {"error": "unauthorized"};		
			controller.loginerror(error);
			break;
	}
  };

  /**
   * Function Comment
   * 
   *
   * @param {String} An access token.
   * @return {string} .
   * @private
   */
   function getPGPKeys_(accessToken) {
	var request = Request({	url: getUserData_,
				onComplete: parseUserDataResponde_, 
				headers: {"Authorization": "OAuth "+accessToken}
           		  }).get();	
  };

  /**
   * Function Comment
   * 
   *
   * @param {String} An access token.
   * @return {string} .
   * @private
   */
  function parseUserDataResponde_(response){
    switch(response.status){
      case 200:
		console.log(response.text);
        var jsonObj = response.json;
        if(jsonObj.hasOwnProperty('objects')){
			// Success we has loged
			userData = jsonObj;
			//TODO: Emit "loginsuccess"
			controller.loginsuccess(userData);
        }else{
          // Check if an error happened or if user does't have keys data
        }
        break;
      default:
        var error = {"error": "unauthorized"};		
	controller.loginerror(error);
	break;
    }    
  };

