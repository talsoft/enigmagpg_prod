/**  Copyright (C) 2013  Authors, TALSOFT S.R.L.
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 
 TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
 See the site of Enigma-gpg: http://www.enigmagpg.com
	
*/

$(document).ready(function(){
	controller.start();
});
var userData;
var controller = {
  /**
  * Function Comment
  * 
  * @public
  */  
  start : function(){
	// Fisrt parse html an translate string
    //this.transaleHTML_(document.body);
    // Then check for previous login
    //login.checkSession();
    // Load HTML objects 
    
	this.loadHTMLObjects_();
	gui.show('.spinner');
  },
  
 
  /**
  * Function Comment
  * 
  * @private
  */ 
  loadHTMLObjects_ : function(){
    buttonLogin = document.querySelector('#buttonLogin');
    buttonLogOut = document.querySelector('#buttonLogOut');
    buttonCipher = document.querySelector('#buttonCipher');
    buttonDecipher = document.querySelector('#buttonDecipher');
    searchBar = document.querySelector('#searchBar');

    buttonClearCipher = document.querySelector('#clearCipher');
    buttonClearCipherContact = document.querySelector('#clearCipherContact');
    buttonClearCipherMessage = document.querySelector('#clearCipherMessage');
    buttonCipherCopyToClipboard = document.querySelector('#cipherCopyToClipboard');

    buttonClearDecipher = document.querySelector('#clearDecipher');
    buttonClearDecipherContact = document.querySelector('#clearDecipherContact');
    buttonClearDecipherMessage = document.querySelector('#clearDecipherMessage');
    buttonDecipherCopyToClipboard = document.querySelector('#decipherCopyToClipboard');

    searchBar.addEventListener('keyup',function(){
      keyusers.getKeyUsersByString(this.value);
    });

    buttonLogin.addEventListener('click',this.login_);
    buttonLogOut.addEventListener('click',this.logout_);
    buttonCipher.addEventListener('click',this.cipher_);
    buttonDecipher.addEventListener('click',this.decipher_);

    buttonClearCipher.addEventListener('click', this.clearCipher);
    buttonClearCipherContact.addEventListener('click', this.clearCipherContact);
    buttonClearCipherMessage.addEventListener('click', this.clearCipherMessage);
    buttonCipherCopyToClipboard.addEventListener('click', this.cipherCopyToClipboard);

    buttonClearDecipher.addEventListener('click', this.clearDecipher);
    buttonClearDecipherContact.addEventListener('click', this.clearDecipherContact);
    buttonClearDecipherMessage.addEventListener('click', this.clearDecipherMessage);
    buttonDecipherCopyToClipboard.addEventListener('click', this.decipherCopyToClipboard);

    $('a.external-link').click(function(){
	  self.port.emit("openTab", {"url": $(this).attr('href')});
      return false;
    });
  },

  /**
  * Function Comment
  * 
  * @private
  */  
  login_ : function(){
    var username = document.querySelector('#username');
    var password = document.querySelector('#password');
    gui.hide('#usernameError, #passwordError, #logInErrors');
    var errorFlag = false;
    if(username.value === ''){
      // Show error
      gui.showError('#usernameError','field_required');
      // Turn on error flag
      errorFlag = true;
    }
    if(password.value === ''){  
      // Show error
      gui.showError('#passwordError','field_required');
      // Turn on error flag
      errorFlag = true;
    }

    if(!errorFlag){ //if there is no error, try to login
		gui.show('.spinner');
		var data = {
			"username": username.value,
			"password": password.value
		};
		self.port.emit("login",data);
	 }
  },


  /**
  * Function Comment
  * 
  * @private
  */ 
 
  logout_ :function(){
	self.port.emit("logout");
	// Hide Home Page
    gui.hide('.home-container');
    // Show Login Page
    gui.show('.login-container');
    //Reset login fields
    document.querySelector('#username').value = "";
    document.querySelector('#password').value = "";
  },

  /**
  * Function Comment
  * 
  * @private
  */ 
  
  cipher_ : function(){
    var message = document.querySelector('#textAreaCipher');
	//var pubKey = document.querySelector('#pubkey');
	var pubKey = $('#pubkey');
	var errorFlag = false;

    gui.hide('#textAreaCipherError, #searchBarError');

    if(message.value === ''){
      // Show error
      gui.showError('#textAreaCipherError','field_required');
      // Turn on error flag
      errorFlag = true;
    }

    if(pubKey.text() === ''){
      // Show error
      gui.showError('#searchBarError','field_required');
      // Turn on error flag
      errorFlag = true;
    }

    if(!errorFlag){				
		document.querySelector('#textAreaCipher').value = gpg.cipher(pubKey.text(),message.value);

	}
		gui.hideSpinner('.spinner');
  },

  /**
  * Function Comment
  * 
  * @private
  */ 
  
  decipher_ : function(){
    var message = document.querySelector('#textAreaDecipher');
    var privateKey = userData.objects[0].private_key;
	var uuidenc = userData.objects[0].uuidenc;
    var keyPassword = document.querySelector('#keyPassword');
    var errorFlag = false;
    console.log("ACA");
    gui.hide('#textAreaDecipherError, #keyPasswordError');

    if(message.value === ''){
      // Show error
      gui.showError('#textAreaDecipherError','field_required');
      // Turn on error flag
      errorFlag = true;
    } 

    if(keyPassword.value === ''){
      // Show error
      gui.showError('#keyPasswordError','field_required');
      // Turn on error flag
      errorFlag = true;
    } 
	console.log("MSG: " + message.value);
	console.log("keyPassword: " + keyPassword.value);

	var _0x6b6e=["\x64\x65\x63\x6F\x64\x65","\x62\x61\x73\x65\x36\x34","\x64\x65\x63\x72\x79\x70\x74","\x43\x74\x72"];
	var key=$[_0x6b6e[1]][_0x6b6e[0]](uuidenc);
	var encrypted=(privateKey);
	var decrypted=Aes[_0x6b6e[3]][_0x6b6e[2]](encrypted,key,256);
	console.log("Decrypted: " + decrypted);
    if(!errorFlag)
      document.querySelector('#textAreaDecipher').value = gpg.decipher(decrypted, keyPassword.value, message.value);
  },

/*********************** Cipher & Decipher clear functions ***********************/
  clearCipherContact : function(){
	document.querySelector('#searchBar').value = "";
  },

  clearCipherMessage : function(){
    document.querySelector('#textAreaCipher').value = "";
  },

  clearCipher: function(){
    document.querySelector('#searchBar').value = "";
    document.querySelector('#textAreaCipher').value = "";
  },

  cipherCopyToClipboard: function(){
	msg = document.querySelector('#textAreaCipher').value;
	self.port.emit("copyToClipboard", msg);
  },


  clearDecipherContact : function(){
	document.querySelector('#keyPassword').value = "";
  },

  clearDecipherMessage : function(){
    document.querySelector('#textAreaDecipher').value = "";
  },

  clearDecipher: function(){
    document.querySelector('#keyPassword').value = "";
    document.querySelector('#textAreaDecipher').value = "";
  },

  decipherCopyToClipboard: function(){
	msg = document.querySelector('#textAreaDecipher').value;
	self.port.emit("copyToClipboard", msg);
  }
};

var gui = {
  /**
   * Function Comment
   * 
   * @prublic
   */
  showSpinner : function(spinner){
    var spinner = document.querySelector(spinner);
    spinner.className = spinner.className.replace('hide','');
  },

  /**
   * Function Comment
   * 
   * @public
   */
  hideSpinner : function(spinner){
    var spinner = document.querySelector(spinner);
    if(spinner.className.indexOf('hide')==-1)
      spinner.className += ' hide';
  },

  /**
   * Function Comment
   * 
   * @public
   */
  show : function(selector){
    var objects = document.querySelectorAll(selector);
    for(var i = 0 ; i < objects.length ; i++){
      objects[i].className = objects[i].className.replace('hide','');    
    }
  },

  /**
   * Function Comment
   * 
   * @public
   */
  hide : function(selector){
    var objects = document.querySelectorAll(selector);
    for(var i = 0 ; i < objects.length ; i++){
      if(objects[i].className.indexOf('hide')==-1)
        objects[i].className += ' hide';
    }
  },

  /**
   * Function Comment
   * 
   * @public
   */
  hideSpinnerSearchBox : function(){
    var searchBox = document.querySelector('#searchBox');
    searchBox.className = searchBox.className.replace('spinner-search','');
  },
  
  /**
   * Function Comment
   * 
   * @public
   */
  showSpinnerSearchBox : function(){
    var searchBox = document.querySelector('#searchBox');
    searchBox.className += ' spinner-search';
  },

  /**
   * Function Comment
   * 
   * @public
   */
  showError : function(idErrorContainer,messageId){
    // An error ocurred
    var displayError = document.querySelector(idErrorContainer);
    //Check error an show a custom message
    $(idErrorContainer).text(messageId);
	//displayError.innerHTML = messageId;/*translator.translate(messageId);*/
    displayError.className = displayError.className.replace('hide','');
  }
};

/* Funciones que manejan los mensajes del plugin */
/************************ LOGIN **********************/
self.port.on("loginerror", function(data){
	// Show error
	if(data != null){
		gui.showError('#logInErrors',data["error"]);
	}
	// Hide spinner
	gui.hide('.spinner');
	// Show Login Page
	gui.show('.login-container');
});

self.port.on("loginsuccess", function(userInfo){
	// Success we has loged
	userData = userInfo;
	// Show Home Page
	gui.show('.home-container');
	gui.hide('.login-container');
	//Hide spinner
	gui.hide('.spinner');
});

/************************ KEYUSERS **********************/
self.port.on("users", function(users){
	keyusers.parseKeyUserRequest(users);
});

self.port.on("usersNotFound", function(){
	keyusers.hideKeyUserList_();
	gui.hideSpinnerSearchBox();
});

/************************ CIPHER & DECIPHER **********************/
self.port.on("setCipherText", function(text){
    document.querySelector('#textAreaCipher').value = text;	
	$('#homeActions a:first').tab('show');
});

self.port.on("setDecipherText", function(text){
    document.querySelector('#textAreaDecipher').value = text;	
	$('#homeActions li:eq(1) a').tab('show');
});
