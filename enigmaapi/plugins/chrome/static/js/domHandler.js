/**  Copyright (C) 2013  Authors, TALSOFT S.R.L.
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 
 TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
 See the site of Enigma-gpg: http://www.enigmagpg.com
	
*/

var domHandler = {

	/**
   	* Function Comment
   	* 
   	*
   	* @public
   	*/

	setSelectedText : function(text){
		var sel, range;
	    var doc = getCurrentDocument(document);
	    if (window.getSelection) {
	        sel = doc.getSelection();
	        var activeElement = doc.activeElement;
	        if (activeElement.nodeName == "TEXTAREA" ||
	           (activeElement.nodeName == "INPUT" && activeElement.type.toLowerCase() == "text")) {
	               var val = activeElement.value, start = activeElement.selectionStart, end = activeElement.selectionEnd;
	               activeElement.value = val.slice(0, start) + text + val.slice(end);
	               return;
			}
		
			if (sel.rangeCount) {
		              range = sel.getRangeAt(0);
		              range.deleteContents();
		              range.insertNode(document.createTextNode(text));
		              return;	
			 }
		         
			sel.deleteFromDocument();
			         
	        
	    }else if (document.selection && document.selection.createRange) {
	        range = document.selection.createRange();
	        range.text = text;
	    }
	},

	/**
   	* Function Comment
   	* 
   	*
   	* @private
   	*/
   
	getCurrentDocument_ : function(document){
	    var activeElem = document.activeElement;
	    if(activeElem.tagName == 'IFRAME'){
	       document =  activeElem.contentDocument; 
	       return this.getCurrentDocument_(document); 
	    }else{
	        return document;
	    }
	},


	getSelectedText :function() {
		
		var doc = this.getCurrentDocument_(document);
	        
		if(doc.activeElement.nodeName === 'TEXTAREA' || doc.activeElement.nodeName === 'INPUT'){
			return 	doc.activeElement.value;		
		}
		
		if (typeof window.getSelection != "undefined") {
		    var sel = doc.getSelection();
		    if (sel.rangeCount) {
			var container = document.createElement("div");
			for (var i = 0; i < sel.rangeCount; ++i) {
			    container.appendChild(sel.getRangeAt(i).cloneContents());
			}
			if(navigator.userAgent.indexOf('Firefox')!=-1){	
				return container.textContent;
			}else{
				return container.innerText;
			}
		    }
		}

		if (typeof document.selection != "undefined") {
			if (document.selection.type == "Text") {
			    return document.selection.createRange().htmlText;
			}
	   	 }
	         
		return '';
	}

};



chrome.extension.onMessage.addListener(function(msg, _, sendResponse) {
	
	if(msg.execute =="getSelectedText"){
        sendResponse({selectedText: domHandler.getSelectedText()});
    }

});
