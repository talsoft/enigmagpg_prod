/**  Copyright (C) 2013  Authors, TALSOFT S.R.L.
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 
 TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
 See the site of Enigma-gpg: http://www.enigmagpg.com
	
*/

var CLIENT_ID = "c0d5c00fd9206e3e72a2";

var CLIENT_SECRET = "fb574ed852fe1d5bf1a4ec0490a5e0bf581ac3c1";

var SCOPE = "write";

var GRANT_TYPE ="password";

var userData;

var login = {

  /**
   * Param Comment
   *
   * @type {string}
   * @private
   */
  getAccessToken_: 'https://app.enigmagpg.com/oauth2/access_token',

  /**
   * Param Comment
   *
   * @type {string}
   * @private
   */

  getUserData_: 'https://app.enigmagpg.com/api/v1/user/?format=json',

  /**
   * Param Comment
   *
   * @type {string}
   * @private
   */
   
   username_: '', 

  /**
   * Param Comment
   *
   * @type {string}
   * @private
   */
  
  password_: '',



  /**
   * Function Comment
   * 
   *
   * @public
   */

  login: function(username,password){
    gui.show('.spinner');
    this.username_ = username;
    this.password_ = password;
    this.requestAccessToken_();
  },
  
  /**
   * Function Comment
   * 
   *
   * @public
   */

  logout: function(){
    // Delete user info
    userData = null;
    // Delete access token
    localStorage.removeItem('accessToken');
    // Hide Home Page
    gui.hide('.home-container');
    // Show Login Page
    gui.show('.login-container');
  },


  /**
   * Function Comment
   * 
   *
   * @public
   */

  checkSession: function(){
    gui.showSpinner('.spinner');
    // Check if we have a previous access token
    if(localStorage.hasOwnProperty("accessToken")){
      this.getPGPKeys_(localStorage.accessToken);  
    }else{
      // We don't have an acess token so show login page
      gui.hide('.spinner');
      // Show Login Page
      gui.show('.login-container');
    }

  },



  /**
   * Function Comment
   * 
   *
   * @private
   */
  requestAccessToken_: function() {
    var xmlhttp = new XMLHttpRequest();
    var data = 'client_id='+CLIENT_ID+'&client_secret='+CLIENT_SECRET+'&grant_type='+GRANT_TYPE+'&username='+this.username_+'&password='+this.password_;
    xmlhttp.onload = this.parseAccesTokenResponde_.bind(this);
    xmlhttp.open("POST",this.getAccessToken_,true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send(data);
  },

  /**
   * Function Comment
   * 
   * 
   *
   * @param {ProgressEvent} e The XHR ProgressEvent.
   * @private
   */
  parseAccesTokenResponde_: function (e) {
    switch(e.target.status){
      case 200:
          var jsonObj = JSON.parse(e.target.responseText);
          if(jsonObj.hasOwnProperty('access_token')){
            // We get access token successfully
            this.getPGPKeys_(jsonObj.access_token);
            // Storage acces token using local storage
            localStorage.accessToken = jsonObj.access_token;
          }else if(jsonObj.hasOwnProperty('error')){
            // Show error
            gui.showError('#logInErrors',jsonObj.error);
            // Hide spinner
            gui.hide('.spinner');
            // Show Login Page
            gui.show('.login-container');
          } 
          break;
      case 401:
          // Hide spinner
          gui.hide('.spinner');
          // Show Login Page
          gui.show('.login-container');
          // Show error
          gui.showError('#logInErrors','unauthorized');
          break;
    }
  },

  /**
   * Function Comment
   * 
   *
   * @param {String} An access token.
   * @return {string} .
   * @private
   */
  getPGPKeys_: function (accessToken) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onload = this.parseUserDataResponde_.bind(this);
    xmlhttp.open("GET",this.getUserData_,true);
    xmlhttp.setRequestHeader("Authorization","OAuth "+accessToken);
    xmlhttp.send();
  },


  /**
   * Function Comment
   * 
   *
   * @param {String} An access token.
   * @return {string} .
   * @private
   */
  

  parseUserDataResponde_: function(e){
    switch(e.target.status){
      case 200:
        var jsonObj = JSON.parse(e.target.responseText);
        if(jsonObj.hasOwnProperty('objects')){
        // Success we has loged
        userData = jsonObj;
        // Show Home Page
        gui.show('.home-container');
        gui.hide('.login-container');
        //Hide spinner
        gui.hide('.spinner');
        }else{
          // Check if an error happened or if user does't have keys data
        }
        break;
      case 401:
        // Hide spinner
        gui.hide('.spinner');
        // Show Login Page
        gui.show('.login-container');
        // Show error
        gui.showError('#logInErrors','unauthorized');
        break;  
    }
    
  },

};
