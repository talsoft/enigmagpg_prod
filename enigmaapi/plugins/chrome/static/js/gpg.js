/**  Copyright (C) 2013  Authors, TALSOFT S.R.L.
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 
 TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
 See the site of Enigma-gpg: http://www.enigmagpg.com
	
*/

var gpg = {


	/**
   	* Function Comment
   	* 
   	*
   	* @public
   	*/

	decipher : function(privateKey, keyPassword, message){
		
		gui.showSpinner('.spinner');

		openpgp.init();

		var privKey = openpgp.read_privateKey(privateKey);
		
		var auxMessage = message;

		auxMessage.replace(translator.translate('generated_by'),'');	
		

		if (privKey.length < 1) {
			//Show error
      		gui.showError('#homeErrors','no_private_key_found');
			// By default return the original message
			return message;
		}
		var msg = openpgp.read_message(auxMessage);
		var keymat = null;
		var sesskey = null;
		// Find the private (sub)key for the session key of the message
		for (var i = 0; i< msg[0].sessionKeys.length; i++) {
			if (privKey[0].privateKeyPacket.publicKey.getKeyId() == msg[0].sessionKeys[i].keyId.bytes) {
				keymat = { key: privKey[0], keymaterial: privKey[0].privateKeyPacket};
				sesskey = msg[0].sessionKeys[i];
				break;
			}
			for (var j = 0; j < privKey[0].subKeys.length; j++) {
				if (privKey[0].subKeys[j].publicKey.getKeyId() == msg[0].sessionKeys[i].keyId.bytes) {
					keymat = { key: privKey[0], keymaterial: privKey[0].subKeys[j]};
					sesskey = msg[0].sessionKeys[i];
					break;
				}
			}
		}
		if (keymat != null) {
			if (!keymat.keymaterial.decryptSecretMPIs(keyPassword)) {
				//Show error
      			gui.showError('#homeErrors','incorrect_private_key_password');
				gui.hideSpinner('.spinner');
				// By default return the original message
				return message;

			}
			message = msg[0].decrypt(keymat, sesskey);
			gui.hideSpinner('.spinner');
			return message;
		} else {
			//Show error
      		gui.showError('#homeErrors','no_private_key_found');
		}
		gui.hideSpinner('.spinner');
		// By default return the original message
		return message;
	},


	/**
   	* Function Comment
   	* 
   	*
   	* @public
   	*/
   
    cipher:function(publicKey,message){
    	gui.showSpinner('.spinner');
    	openpgp.init();
    	var pub_key = openpgp.read_publicKey(publicKey);
		if (pub_key < 1) {
			//Show error
      		gui.showError('#homeErrors','no_public_key_found');
			gui.hideSpinner('.spinner');
			return message;
		}
		message = openpgp.write_encrypted_message(pub_key,message);
		gui.hideSpinner('.spinner');
		return message+'\n'+translator.translate('generated_by');
    }
};