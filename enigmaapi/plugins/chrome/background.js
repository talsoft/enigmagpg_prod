/**  Copyright (C) 2013  Authors, TALSOFT S.R.L.
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 
 TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
 See the site of Enigma-gpg: http://www.enigmagpg.com
	
*/

var selectedText = "";
/**
* Function Comment
* 
*
* @public
*/
var contextMenu = {
	
	/**
	* Param Comment
	*
	* @type {string}
	* @private
	*/
	urlPopUp_: '/templates/plugin.html', 

	/**
	* Param Comment
	*
	* @type {string}
	* @private
	*/
	popUpWidth_: 800, 
	
	/**
	* Param Comment
	*
	* @type {string}
	* @private
	*/
	popUpHeight_: 600, 
	
	/**
	* Function Comment
	* 
	*
	* @public
	*/

	cipher: function(){
		// Set hash tag
		var url = contextMenu.urlPopUp_+'#'+'cipher';
		url = chrome.extension.getURL(url);
		var w = contextMenu.popUpWidth_;
		var h = contextMenu.popUpHeight_;
		var left = (screen.width/2)-(w/2);
		var top = (screen.height/2)-(h/2);
		popupOption = {url:url,focused:true,type:"popup", 'width': w, 'height': h, 'left': left, 'top': top};
		contextMenu.openWindow_(popupOption);
	},

	/**
	* Function Comment
	* 
	*
	* @public
	*/

	decipher: function(){
		// Set hash tag
		var url = contextMenu.urlPopUp_+'#'+'decipher';
		url = chrome.extension.getURL(url);
		var w = contextMenu.popUpWidth_;
		var h = contextMenu.popUpHeight_;
		var left = (screen.width/2)-(w/2);
		var top = (screen.height/2)-(h/2);
		popupOption = {url:url,focused:true,type:"popup", 'width': w, 'height': h, 'left': left, 'top': top};
		contextMenu.openWindow_(popupOption);
	},

	openWindow_ : function(popupOption){
		chrome.tabs.getSelected(null, function(tab) {
			chrome.tabs.sendMessage(tab.id, {execute:"getSelectedText"}, function(response) {
                try{
	                var text = response.selectedText;
	                selectedText = text;
	                chrome.windows.create(popupOption, function(){});
	                return; 
	            }catch(e){
            		console.log('error',e);
               		alert(chrome.i18n.getMessage("dialogErrorUndefinedSelectedText"));		 	
            	}
            });
        });	
	}

};

/**
* Function Comment
* 
*
* @public
*/

chrome.browserAction.onClicked.addListener(function(tab) {
	chrome.browserAction.setPopup({popup:'/templates/plugin.html'});
});



/**
* Function Comment
* 
*
* @public
*/
var contexts = [ "page", "frame", "selection"];
var parent = chrome.contextMenus.create({"title": chrome.i18n.getMessage("enigma_gpg"),"contexts":contexts});
var childCipher = chrome.contextMenus.create(
  {"title": chrome.i18n.getMessage("cipher_text"), "parentId": parent, "onclick": contextMenu.cipher,"contexts":contexts});
var childDecipher = chrome.contextMenus.create(
  {"title": chrome.i18n.getMessage("decipher_text"), "parentId": parent, "onclick": contextMenu.decipher,"contexts":contexts});


