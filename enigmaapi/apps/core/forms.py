#  Copyright (C) 2013  Authors, TALSOFT S.R.L.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# 
# TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
# See the site of Enigma-gpg: http://www.enigmagpg.com

from django import forms as forms
from enigmaapi.apps.core.models import UserProfile
from passwords.fields import PasswordField
from registration.backends.default.views import RegistrationView
from registration.forms import RegistrationFormUniqueEmail
from enigmaapi import settings


class SafeRegistrationForm( RegistrationFormUniqueEmail):
    
    bad_domains = [ 'mailinator.com','yopmail.com','filzmail.com','drdrb.com','mintemail.com','hushmail.com']
    
    def clean( self):
        """ Tests the password for dictionary attacks. """
        r = super( SafeRegistrationForm, self).clean()
        if r:
            import crack
            try:
                crack.VeryFascistCheck( self.cleaned_data[ 'password1'])
            except ValueError, message:
                raise forms.ValidationError("Please use a stronger password to protect your account. The current one is too weak")
            # Checkusername Min length
            try:
                username = self.cleaned_data.get('username')
                if username is not None and len(username) <= 4:
                    raise forms.ValidationError("Username must be at least 5 chars.")
            except ValueError, message:
                None
            # check mail service temporaly
            email_domain = self.data['email'].split('@')[1]
            if email_domain in self.bad_domains:
                raise forms.ValidationError("Registration using free email addresses is prohibited. Please supply a different email address.")
            if settings.DOMAIN_ALLOW_REGISTER is not "" and email_domain not in settings.DOMAIN_ALLOW_REGISTER:
                raise forms.ValidationError("Registration for that domain is prohibited. Please supply a different email address.")
            return r

class RegistrationViewUniqueEmail(RegistrationView):
    form_class = SafeRegistrationForm


class UserProfileForm(forms.ModelForm):
    first_name = forms.CharField()
    last_name = forms.CharField()
    
    class Meta:
        model   = UserProfile
        exclude = ["user","email","date_joined","invited_user","membership","public_key","private_key","last_ip"]
    
   
    
    def __init__(self, *args, **kwargs):
        super(UserProfileForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs['class'] = 'inputform'
        self.fields['last_name'].widget.attrs['class'] = 'inputform'  
        self.fields['site'].widget.attrs['class'] = 'inputform'
        #self.fields['email'].widget.attrs['class'] = 'inputform'
        self.fields['company'].widget.attrs['class'] = 'inputform'
        self.fields['securitykeys'].widget.attrs['class'] = 'inputform'
        #self.fields['last_ip'].widget.attrs['class'] = 'inputform'  
    

class UserInviteForm(forms.Form):
    email = forms.EmailField
    

class UserMessageForm(forms.Form):
    email = forms.EmailField
    message = forms.Textarea

class UserDecipherMessageForm(forms.Form):
    keypassword = forms.PasswordInput
    message = forms.Textarea
        
class Request_passwordkey_form(forms.Form):
    keypassword =  PasswordField()

    
    
    