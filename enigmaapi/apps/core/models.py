#  Copyright (C) 2013  Authors, TALSOFT S.R.L.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# 
# TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
# See the site of Enigma-gpg: http://www.enigmagpg.com

from django.contrib.auth.models import User
from tastypie.cache import SimpleCache
from django_extensions.db.fields import UUIDField
from django.db import models
from django.core.urlresolvers import reverse
from django.core.mail.message import EmailMultiAlternatives
from datetime import datetime
from enigmaapi.apps.core.gnupgp import gpgBackend
from django.template.loader import render_to_string
import base64
from django.utils import timezone
from enigmaapi import settings



try:
    from django.contrib.auth import get_user_model
    User = get_user_model()
except ImportError:
    from django.contrib.auth.models import User

try:
    from django.utils.timezone import now as datetime_now
except ImportError:
    datetime_now = datetime.datetime.now

try:
    from django_extensions.db.fields.encrypted import EncryptedTextField, EncryptedCharField
except ImportError:

    class EncryptedCharField():
        def __init__(self, **kwargs):
            pass

    class EncryptedTextField():
        def __init__(self, **kwargs):
           pass

# Create your models here.

SECURITYKEYS = (
    (u'D', u'Default'),
    (u'I', u'Intermediate'),
    (u'P', u'Paranoic'),
)
MEMBERSHIP = (
    (u'U', u'User'),
    (u'D', u'Developer'),
    (u'B', u'Business'),
)

class UserProfile(models.Model):
   
    uuid_field = UUIDField(blank=True, null=True, auto=True)
    user = models.ForeignKey(User, related_name='user')
    site = models.URLField()
    email = models.EmailField()
    company = models.CharField(max_length=100)
    membership = models.CharField(max_length=2, choices=MEMBERSHIP)
    securitykeys = models.CharField(max_length=2, choices=SECURITYKEYS)
    date_joined = models.DateField()
    # last renew keys
    lastRenewKeys = models.DateTimeField(auto_now_add=True)
    # User Invited 
    invited_user = models.ForeignKey(User, related_name='invited_user',blank=True, null=True)
    # last IP access
    last_ip = models.IPAddressField(null=True,blank=True)
    # Encrypted Fields
    public_key = EncryptedTextField(max_length=60000)
    private_key = EncryptedTextField(max_length=60000)
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.user.email
    # Add it here.
    cache = SimpleCache(timeout=10)
    
    def get_absolute_url(self):
        return reverse('userProfileView')

    def get_nextGenerateKeysDays(self):
        days = timezone.now() - self.lastRenewKeys
        if self.securitykeys=="D":
            return 365 - days.days
        else:
            if self.securitykeys=="I":
                return 7 - days.days
            else:
                if self.securitykeys=="P":
                    return 2 - days.days
                else: 
                    return 0
    
    # Verify is regenerate key of user   
    def check_regenerateKeysDays(self):
        days = timezone.now() - self.lastRenewKeys
        if self.securitykeys=="D" and days.days >= 365:
            return True
        else:
            if self.securitykeys=="I" and days.days >= 7:
                return True
            else:
                if self.securitykeys=="P" and days.days >= 2:
                    return True
                else:
                    return False
        return False
    
    # generate key pair with keypassword
    def generateKeysPair(self,userprofile,keypassword,username,email):
        # generate key
        key = gpgBackend().generate_multiple_key(userprofile.securitykeys,keypassword,username,email)
        
        result = key.get_checks_list().get('result').get('status')
        
        if not result=="generated_keys":
            return result
        
        # regenerate keys
        userprofile.private_key= gpgBackend().get_key_to_text(userprofile.email, True)
        userprofile.public_key= gpgBackend().get_key_to_text(userprofile.email, False)
        userprofile.lastRenewKeys=datetime.now()
        userprofile.save()
        # delete key from gpg
        gpgBackend().delete_key(userprofile.email, True)
        gpgBackend().delete_key(userprofile.email, False)
        
        return result
    
    # encrypt text with aes
    # Convert to base64 the password and plaintext
    # then encrypt 
    def encrypt(self,password,plaintext):
        import aes
        
        key = password
        message = plaintext 
        blocksize = 256   # can be 128, 192 or 256
        # encrypt text
        crypted = aes.encrypt( message, key, blocksize )

        return crypted
        
    
class KeyContainer(models.Model):
    #UUID Keycontainer
    user = models.ForeignKey(UserProfile, related_name='userkey')
    userContact = models.ForeignKey(UserProfile, related_name='keyContact')
    created_at = models.DateTimeField(auto_now_add=True)
    
    # Add it here.
    cache = SimpleCache(timeout=10)
    
    def __unicode__(self):
        return self.user.user.username + "|" + self.userContact.user.username
        
class InviteProfile(models.Model):
    
    userprofile = models.ForeignKey(UserProfile, related_name='userprofile')
    invitation_email = models.EmailField()
        
    def __unicode__(self):
        return u"Invitation information for %s" % self.userprofile
    
    # verify invited user and setting in key container
    def search_user_request_invited(self,user_logged,userprofile):
        try:
            
            user_request_invite = InviteProfile.objects.get(invitation_email=user_logged.email)
            #get user request and create container user invited
            keycontainerInvited = KeyContainer()
            keycontainerInvited.user=user_request_invite.userprofile
            keycontainerInvited.userContact=userprofile
            keycontainerInvited.save()
            #set keycontainer to user invited
            #keycontainerUserInvited = KeyContainer.objects.get(userContact=user_request_invite.userprofile)
            
            keycontainerRequest = KeyContainer()
            keycontainerRequest.user=userprofile
            keycontainerRequest.userContact=user_request_invite.userprofile
            keycontainerRequest.save()
            
            # Update invite user 
            profileUserInviteUser = userprofile
            profileUserInviteUser.invited_user=user_request_invite.userprofile.user
            profileUserInviteUser.save()
            # delete invitation
            InviteProfile.delete(user_request_invite)       
        except InviteProfile.DoesNotExist:
            user_request_invite = None
        return None
    
    def send_activation_email(self,user,form):
        
        ctx_dict = {'user_invite': user.first_name + ' ' + user.last_name}
        
        subject = render_to_string('invitation/invitation_email_subject.txt',
                                   ctx_dict)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        
        message = render_to_string('invitation/invitation_email.txt',
                                   ctx_dict)
        
        mail = EmailMultiAlternatives(subject, message, user.email, [form.data['email']])
    
        mail.attach_alternative(message, "text/html") 
        
        mail.send()
    
    def send_activation_user(self,user):
        
        ctx_dict = {'user_invite': user.first_name + ' ' + user.last_name + ' - '+user.email}
        
        subject = render_to_string('registration/activation_user_email_subject.txt',
                                   ctx_dict)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        
        message = render_to_string('registration/activation_user_email.txt',
                                   ctx_dict)
        
        # Send notification when an user is activate into the system
        mailFrom = settings.NOTIFY_ACTIVATION_MAILFROM
        mailTo = settings.NOTIFY_ACTIVATION_MAILTO
        mail = EmailMultiAlternatives(subject, message,mailFrom, [mailTo])
    
        mail.attach_alternative(message, "text/html") 
        
        mail.send()
        