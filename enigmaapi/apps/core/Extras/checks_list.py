#!/usr/bin/python
# -*- coding: utf-8 -*-

#  Copyright (C) 2013  Authors, TALSOFT S.R.L.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Authors                                                             
# Mailen Horbulewicz, mailen.horbul@gmail.com
# Maximo Martinez, maximomrtnz@gmail.com
# Pablo Meyer, pablitomeyer@gmail.com
# 
# Graduation Project at FASTA University - Mar del Plata, Argentina
# TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
# See the site of enigma-gpg: http://www.talsoft.com.ar/index.php/research/tools/enigma-gpg

## @module check_list.py
# This script has the Class that represents the Json Object that We will  
# send to response a request


class ChecksList(object):
	
	##docstring for ListaChecks
	def __init__(self):
		self.__result = {}
		self.__errors = []
		self.__validations = {}
	
	def add_validation(self,validation,value):	
		self.__validations[validation] = value

	def add_error(self,error):
		self.__errors.append(error)

	def add_result(self,result,value):
		self.__result[result] = value	
	
	def get_checks_list(self):
		return {'result':self.__result,'errors':self.__errors,'validations':self.__validations}
		
	def is_valid(self):	
		i = 0
  		values = self.__validations.values()
 		while i < len(values) and values[i]:
    			i += 1
  		return i == len(values)

