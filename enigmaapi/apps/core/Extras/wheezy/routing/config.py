
""" ``config`` module.
"""


from Servidor.Extras.wheezy.routing.curly import default_pattern as curly_default_pattern
from Servidor.Extras.wheezy.routing.curly import patterns as curly_patterns
from Servidor.Extras.wheezy.routing.curly import try_build_curly_route
from Servidor.Extras.wheezy.routing.plain import try_build_plain_route
from Servidor.Extras.wheezy.routing.regex import try_build_regex_route
from Servidor.Extras.wheezy.routing.choice import try_build_choice_route


assert curly_default_pattern
assert curly_patterns

route_builders = [
    try_build_plain_route,
    try_build_choice_route,
    try_build_curly_route,
    try_build_regex_route
]
