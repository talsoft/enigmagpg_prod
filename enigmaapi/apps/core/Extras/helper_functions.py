# coding: utf-8

#  Copyright (C) 2013  Authors, TALSOFT S.R.L.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Authors                                                             
# Mailen Horbulewicz, mailen.horbul@gmail.com
# Maximo Martinez, maximomrtnz@gmail.com
# Pablo Meyer, pablitomeyer@gmail.com
# 
# Graduation Project at FASTA University - Mar del Plata, Argentina
# TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
#See the site of enigma-gpg: http://www.talsoft.com.ar/index.php/research/tools/enigma-gpg


## @module helper_functions.py
#  This script has functions to validate user input.


import re
import html_parser
import os 


## Module that checks if an email is valid
def is_email_ok(email):
	return True if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$",email ) else False


## Module that checks if a domain name is valid
def is_domain_name_ok(domain_name):
	return True if re.match("^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}$",domain_name) else False

## Module that checks if a key id is valid
def is_key_id_ok(key_id):
	return True if re.match("^[A-Z0-9]+$",key_id) else False

## Module that checks if a name is valid
def is_name_ok(name):
	return True if ( len(name) > 4) and (name.find('<')==-1 or name.find('>')==-1 )  else False

## Module that checks if an expiration date is valid
def is_expiration_date_ok(expiration_date):
	return True if re.match('^[0-9]{4}-(1[0-2]|0[1-9])-(3[01]|[12][0-9]|0[1-9])$',expiration_date) or re.match('0|\d+[d|w|y|m]$',expiration_date) or re.match('seconds=\d+$',expiration_date) else False

## Module that checks if the dictionary has errors
def is_no_dic_error(dict):
  i = 0
  values = dict.values()
  while i < len(values) and values[i]:
    i += 1
  return i == len(values)

## Module that removes all malicious code from user input
def parameter_cleaner(param):
	## HTML cleaner
	parser = html_parser.HTMLScape()
	parser.feed(param)
	data = parser.get_data()	

	## XSS cleaner
	xss_c = html_parser.XssCleaner()
	data = xss_c.strip(data)			
	
	return data

## Module that deletes files stored in the server
def delete_files(folder):
	for the_file in os.listdir(folder):
    		file_path = os.path.join(folder, the_file)
    		try:
       			if os.path.isfile(file_path):
        			os.unlink(file_path)
    		except Exception, e:
        		return False
    	return True   