# -*- coding: utf-8 -*-

#  Copyright (C) 2013  Authors, TALSOFT S.R.L.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Authors                                                             
# Mailen Horbulewicz, mailen.horbul@gmail.com
# Maximo Martinez, maximomrtnz@gmail.com
# Pablo Meyer, pablitomeyer@gmail.com
# 
# Graduation Project at FASTA University - Mar del Plata, Argentina
# TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
# See the site of enigma-gpg: http://www.talsoft.com.ar/index.php/research/tools/enigma-gpg


## @module enigma_gpg
#  This script has methods to manage an instance of GnuPG

import gnupg
import os

## Method that manages a Singleton Class of GnuPG
#  @param gnupg.GPG
#  @result Operation result: an instance, a path to GnuPg Home.
class Gpg(gnupg.GPG):
	_instance = None
	gnupghome = ''
	def __new__(cls):
		print 'Cls._instance = ' + str(cls._instance)
		if not cls._instance:				
				cls._instance = super(Gpg,cls).__new__(cls)
				print "Creando GPG"
		#else:
		#	super(Gpg,cls).gnupghome = cls.gnupghome
		return cls._instance

	def set_gnupg_home(self, path):
		self.gnupghome = str(path)
		if self.gnupghome and not os.path.isdir(self.gnupghome):
			os.makedirs(self.gnupghome,0x1C0)

	def get_gnupg_home(self):
		return self.gnupghome

## Method that tests the Singleton Class of GnuPG
def test():
	print "--------------- Initiating --------------"
	print "> Creating instance G"
	## We create the first instance
	g = Gpg()
	print g
	print "> g.get_gnupg_home = " + str(g.get_gnupg_home())
	print "> Setting gnupghome to '/home/pablo/Escritorio/prueba'..."
	print "	>> Verifying if the directory exists: " + str(os.path.isdir('/home/pablo/Escritorio/prueba'))
	if os.path.isdir('/home/pablo/Escritorio/prueba'):
		print "	>> Erasing directory..."
		helper_delete('/home/pablo/Escritorio/prueba')
		if os.path.isdir('/home/pablo/Escritorio/prueba'):
			print_error("Error trying to erase the directory")
			return;
		print "	>> Directory deleted"
	g.set_gnupg_home('/home/pablo/Escritorio/prueba')
	print "	>> Gnupghome directory? " + str(g.get_gnupg_home())
	if os.path.isdir('/home/pablo/Escritorio/prueba'):
		print_ok("	>> Directory '/home/pablo/Escritorio/prueba' created successfully")
	else:
		print_error("Error: The directory '/home/pablo/Escritorio/prueba' should have been created")
		return
		
	## print "> Creating a test key..."
	## input_data = input_data = g.gen_key_input(key_type="RSA", key_length=1024)
	## key = g.gen_key(input_data)
	## print "	>> Created key " + str(key)
	## print "> Listing keys: "
	## print "	>> Ammount of keys: " + str(len(g.list_keys()))	

	## We create the second instance.... it should return the same object of the first one
	print ""
	print "> Obtein an other instance of Gpg"
	p = Gpg()
	print "> Verify that it's the same instance "
	print "	>> g = " + str(g)
	print "	>> p = " + str(p)
	print "	>> p is g? " + str(p is g)
	print "	>> g is p? " + str(g is p)
	print " >> g == p? " + str (g == p)
	print_ok("	>> g y p are the same object")
	if ((p is not g) or (g is not p) or (g != p)):
		print_error("Error: They are not the same objet!!")
		return

	print "> Verifying that gnupghome remains"
	print "	>> p.get_gnupg_home = " + str(p.get_gnupg_home()) 
	if (str(p.get_gnupg_home()) != '/home/pablo/Escritorio/prueba'):
		print_error("Error: gnupghome shold be '/home/pablo/Escritorio/prueba'")
		return

	return print_ok("Test pass");
	

def helper_delete(dirname):
	for root, dirs, files in os.walk(dirname, topdown=False):
		## delete all files
		for name in files:
			os.remove(os.path.join(root,name))
		## delete all sub-directories
		for name in dirs:
			os.rmdir(os.path.join(root, name))
	## now we can erase the directory
	os.rmdir(dirname)

def print_error(msg):
	print '\033[1;31m' + str(msg) +'\033[1;m'
	print '\033[1;31mTest Failed \033[1;m'

def print_ok(msg):
	print '\033[1;32m' + str(msg) +'\033[1;m'
