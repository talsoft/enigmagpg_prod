# -*- coding: utf-8 -*-

#  Copyright (C) 2013  Authors, TALSOFT S.R.L.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Authors                                                             
# Mailen Horbulewicz, mailen.horbul@gmail.com
# Maximo Martinez, maximomrtnz@gmail.com
# Pablo Meyer, pablitomeyer@gmail.com
# 
# Graduation Project at FASTA University - Mar del Plata, Argentina
# TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
# See the site of enigma-gpg: http://www.talsoft.com.ar/index.php/research/tools/enigma-gpg


## @module decrypt
#  This script has methods to manage decryption

import os
import gnupg
import sys

try:
	gpg = gnupg.GPG()
except ValueError as error:
	print "GPG application is not installed"
	print error
	sys.exit()

import sys
sys.path.append("../Extras")
from ..Extras.checks_list import ChecksList

## Method that decrypts an encrypted text
#  @param text Text to decrypt
#  @param password Receiver's password
#  @result Operation status, decrypted text
def decrypt_text(text, password):

	checks_list = ChecksList()

	try:
		result = gpg.decrypt(text, passphrase=password)
		checks_list.add_result('status', result.status)
		if result.ok:
			var = result.data
			var.decode('utf-8')
			checks_list.add_result('data', result.data)
		else:
			checks_list.add_error('no_decrypt')
	except UnicodeDecodeError:
		checks_list.add_error('unicode_encode_error')
	except :
		checks_list.add_error('unknown_error')
	return checks_list

## Method that decrypts an encrypted file
#  @param input_file_path File's path to decrypt
#  @param output_file_path File's path to store the result of decryption
#  @param password Receiver's password
#  @result Operation status, decrypted file
def decrypt_file(input_file_path, output_file_path, password):	
	checks_list = ChecksList()
	try:
		stream = open(input_file_path, "rb")
		result = gpg.decrypt_file(stream, output=output_file_path, passphrase=password)
		checks_list.add_result('status', result.status)
		if not result.ok:
		
			checks_list.add_error('no_decrypt')
		else:
			checks_list.add_result('data',output_file_path)	
	except IOError:
		## File error
		checks_list.add_error('io_file_error')
	except :
		## Error
		checks_list.add_error('unknown_error')	
	return checks_list
	
## Method that decrypts and verifies an encrypted text
#  @param text Text to decrypt
#  @param password Receiver's password
#  @result Operation status, decrypted text
def decrypt_verify_text(text, password):
	checks_list = ChecksList()
	try:
		result = gpg.decrypt(text, passphrase=password)
		checks_list.add_result('status', result.status)
		
		if not result.ok:
			checks_list.add_error('no_decrypt_valid')
		
		## Sign information
		checks_list.add_result('valid',  result.valid)
		checks_list.add_result('fingerprint', result.fingerprint)
		checks_list.add_result('signature_id', result.signature_id)
		checks_list.add_result('username', result.username)
		checks_list.add_result('trust_text', result.trust_text)
		checks_list.add_result('trust_level', result.trust_level)
		checks_list.add_result('key_id', result.key_id)
		checks_list.add_result('pubkey_fingerprint', result.pubkey_fingerprint)
		checks_list.add_result('expire_timestamp', result.expire_timestamp)
		checks_list.add_result('sig_timestamp', result.sig_timestamp)
		
		## Decrypt information
		var = result.data
		var.decode('utf-8')
		checks_list.add_result('data', result.data)

	except UnicodeDecodeError:
		checks_list.add_error('unicode_encode_error')		
		
	except :
		checks_list.add_error('unknown_error')
			
	return checks_list
	

## Method that decrypts an encrypted text and returns it as a file
#  @param text Text to decrypt
#  @param output_file_path File's path to store the result of decryption
#  @param password Receiver's password
#  @result Operation status, decrypted text stored inside a file
def decrypt_text_to_file(text, output_file_path, password):

	checks_list = ChecksList()
	try:
		result = gpg.decrypt(text, passphrase=password, output=output_file_path)
		checks_list.add_result('status', unicode(str(result.status)))
		if not result.ok:
			checks_list.add_error('no_decrypt')
		else:
			checks_list.add_result('data',output_file_path)	
	except IOError:
		## File error
		checks_list.add_error('io_file_error')
	except :
		## Error
		checks_list.add_error('unknown_error')	
	return checks_list	