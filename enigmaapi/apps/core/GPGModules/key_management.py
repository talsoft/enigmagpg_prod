# coding: utf-8

#  Copyright (C) 2013  Authors, TALSOFT S.R.L.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Authors                                                             
# Mailen Horbulewicz, mailen.horbul@gmail.com
# Maximo Martinez, maximomrtnz@gmail.com
# Pablo Meyer, pablitomeyer@gmail.com
# 
# Graduation Project at FASTA University - Mar del Plata, Argentina
# TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
#See the site of enigma-gpg: http://www.talsoft.com.ar/index.php/research/tools/enigma-gpg


## @module key_management.py
#  This script has methods to manage keys. 

import sys
import gnupg
import json
import ast
import re


## ChecksList import
from enigmaapi.apps.core.Extras.checks_list import ChecksList

from enigmaapi.apps.core.Extras.helper_functions import is_domain_name_ok, is_email_ok, is_expiration_date_ok, is_key_id_ok


## From helper_functions import is_dic_no_error 
try:
	gpg = gnupg.GPG()
except ValueError as error:
	print "GPG application is not installed"
	print error
	sys.exit()
	

## Method that gives back a list of private or public key that were stored inside GnuPG
# @param is_private True if you want to get a list of private keys or False on the other way
# @result Key list
def list_keys(is_private):
	checks_list = ChecksList()
	try:
		keys = gpg.list_keys(is_private)
		checks_list.add_result('data',keys)
	except:
		checks_list.add_error('unknown_error')
    
	return checks_list

## Method that stores a public/private key from text
# @param key Public or Private Key as text
# @result Key's import status
def import_key_from_text(key):

	checks_list = ChecksList()    	
	try:	
		import_result = gpg.import_keys(key)
		if import_result.count == 1:
			checks_list.add_result('status','imported_private_key') if import_result.sec_imported == 1 else False
			checks_list.add_result('status','imported_public_key') if import_result.imported == 1 else False
			gpg.set_trust_level(import_result.fingerprints[0], 6)
		checks_list.add_error('duplicated_private_key') if import_result.sec_dups == 1 else False
		checks_list.add_error('duplicated_public_key') if import_result.unchanged == 1 else False
		checks_list.add_error('no_import') if import_result.count == 0 else False		
		checks_list.add_result(import_result.results) if import_result.sec_imported == 1 else False
	except:
		checks_list.add_error('unknown_error')
	return checks_list

## Method that stores a public/private key from a file
# @param file_path File's path that contains the public/private key to store
# @result Key's import status
def import_key_from_file(file_path):
    
	checks_list = ChecksList()
	try:	
		key = open(str(file_path),'rb').read()
		import_result = gpg.import_keys(key) 
		if import_result.count == 1:
			checks_list.add_result('status','imported_private_key') if import_result.sec_imported == 1 else False
			checks_list.add_result('status','imported_public_key') if import_result.imported == 1  else False
			gpg.set_trust_level(import_result.fingerprints[0], 6)
		checks_list.add_error('duplicated_private_key') if import_result.sec_dups == 1 else False
		checks_list.add_error('duplicated_public_key')if (import_result.unchanged == 1 and import_result.sec_imported != 1) else False
		checks_list.add_error('no_import') if import_result.count == 0 else False
    
	except IOError:
		checks_list .add_error('input_file_error')
	except:
		checks_list .add_error('unknown_error')
	finally:   
		return checks_list

## Method that stores a public/private key from a pgp server
# @param server_url Server URL
# @param key_id Key Id 
# @result Key's import status
def import_key_from_server(server_url,key_id):
	checks_list = ChecksList()
	try:
		checks_list.add_validation('domain_name_ok',is_domain_name_ok(server_url))
		checks_list.add_validation('key_id_ok', is_key_id_ok(key_id))		
		if checks_list.is_valid() :
			import_result = gpg.recv_keys(server_url,key_id)
			if import_result.count == 1:
				checks_list.add_result('status','imported_private_key') if import_result.sec_imported == 1 else False
				checks_list.add_result('status','imported_public_key') if import_result.imported == 1 else False
				gpg.set_trust_level(import_result.fingerprints[0], 6)
			checks_list.add_error('duplicated_private_key') if import_result.sec_dups == 1 else False
			checks_list.add_error('duplicated_public_key') if import_result.unchanged == 1 else False
			checks_list.add_error('no_import_server') if import_result.count != 1 else False
	except:
		checks_list.add_error('unknown_error')
	return checks_list

## Method that returns a public/private key as a text
# @param recipient The key owner's email
# @param is_private  True if you want to get the recipient's private key or False on the other way
# @result public/private key as text
def export_key_to_text(recipient,is_private):
	checks_list = ChecksList()
	try:
		checks_list.add_validation('email',is_email_ok(recipient))  
		if checks_list.is_valid() :
			key = gpg.export_keys(recipient,is_private)
			if key=="":
				checks_list.add_error('no_export')
			else:
				checks_list.add_result('data', key) #Solo agrega si es todo valido
	except:
		checks_list.add_error('unknown_error')

	return checks_list

## Method that returns a public/private key as a file
# @param recipient The key owner's email
# @param file_path File's path to store the key
# @param is_private  True if you want to get the recipient's private key or False on the other way
# @result public/private key as file
def export_key_to_file(recipient,file_path,is_private):
	checks_list = ChecksList()
	try:
		checks_list.add_validation('email', is_email_ok(recipient))
		if checks_list.is_valid():
			key = gpg.export_keys(recipient,is_private)
			if key=="":
				checks_list.add_error('no_export')
			else:
				with open(file_path, 'w') as f:
					f.write(key)
				checks_list.add_result('status','exported_key')
				checks_list.add_result('data',file_path)	
	except IOError:
		checks_list.add_error('output_file_error')
	except :
		checks_list.add_error('unknown_error')
	finally: 
		return checks_list


## Method that deletes a stored public/private key 
# @param recipient The key owner's email
# @param is_private  True if you want to delete the recipient's private key or False on the other way
# @result public/private key deleted status
def delete_key(recipient,is_private):
	checks_list = ChecksList()

	try:
		checks_list.add_validation('email',is_email_ok(recipient))   
		if checks_list.is_valid():
			keys = gpg.list_keys(is_private)
			if len(keys) > 0:
			## Delete 'u' characters wich python add to dictionaries
				keys = ast.literal_eval(json.dumps(keys))
				i = 0
				while i < len(keys):
					## Do while until find the recipient
					email = keys[i]['uids'][0]
					email = email[email.find("<")+1:email.find(">")]
					if email <> recipient:
							i+=1
					else:
						break
				## If We find the recipient, We need to delete it by its fingerprint
				if i < len(keys) and email == recipient:	
					result = gpg.delete_keys(keys[i]['fingerprint'],is_private)
					checks_list.add_result('status','deleted_key') if result.status == 'ok' else False
					checks_list.add_error('exist_private_key') if result.status == 'Must delete secret key first' else False
					checks_list.add_error(result.status) if result.status != 'ok' else False
				else:
					checks_list.add_error('no_key')
			else:
				checks_list.add_error('no_key')
	except :
		checks_list.add_error('unknown_error')

	return checks_list

## Method that generates a simple key. We can use this kind of key only to sign.
# @param key_type Key's type to generate, The key type can be RSA or DSA.
# @param key_length Key's length in bytes. Can be DSA(from 1024 to 3072) or RSA(from 1024 to 4096)  
# @param expiration_date Date when the key will expire  
# @param password Password to keep the key safe
# @param name The name of the key's owner 
# @param comment An additional comment
# @param email The email of the key's owner
# @result Opetation Status
def generate_simple_key(key_type, key_length,expiration_date,password, name, comment, email):
	checks_list = ChecksList()

	try:
		checks_list.add_validation('valid_key_type', True if re.match("RSA|DSA",key_type) else False)
		checks_list.add_validation('key_length',True if (key_type == "RSA" and (1024 <= int(key_length) <=4096)) or (key_type == "DSA" and (1024 <= int(key_length) <=3072)) else False)
		checks_list.add_validation('name', True if ( len(name) > 4) and (name.find('<')==-1 or name.find('>')==-1 )  else False) 
		checks_list.add_validation('email',is_email_ok(email))  
		checks_list.add_validation('expire_date',is_expiration_date_ok(expiration_date))
		if checks_list.is_valid() :
		#	gpg = gnupg.GPG()
			key1 = gpg.gen_key_input(key_type=key_type,expire_date = expiration_date, passphrase = password, key_length=key_length, name_real = name,name_email = email)
			key = gpg.gen_key(key1)
			if key:
				checks_list.add_result('status','generated_key')
			else:
				checks_list.add_error('no_generated_key')
					
	except:
		checks_list.add_error('unknown_error')   

	return checks_list

## Method that generates a multiple key. We use this method to generate a key and a sub key. 
# @param key_type Key's type to generate, The key type can be RSA or DSA.
# @param key_length Key's length in bytes. Can be DSA(from 1024 to 3072) or RSA(from 1024 to 4096)  
# @param expiration_date Date when the key will expire  
# @param password Password to keep the key safe
# @param name The name of the key's owner 
# @param comment An additional comment
# @param email The email of the key's owner
# @param subkey_type Subkey's type to generate, The subkey type can be RSA or ElGamal.
# @param subkey_length The subkey's length in bytes. Can be RSA(from 1024 to 4096) or ElGamal(from 1024 to 3072)  
# # @result Opetation Status
def generate_multiple_key(key_type, key_length,expiration_date,password,name, comment, email,subkey_type,subkey_length):
	checks_list = ChecksList()
	try:
		checks_list.add_validation('valid_keys_type',True if (key_type == "RSA" and subkey_type == "RSA") or (key_type == "DSA" and subkey_type == "ELG-E") else False)
		checks_list.add_validation('key_length',True if (key_type == "RSA" and (1024 <= int(key_length) <=4096)) or (key_type == "DSA" and (1024 <= int(key_length) <=3072)) else False)
		checks_list.add_validation('subkey_length', True if (subkey_type == "RSA" and 1024 <= int(subkey_length) <=4096) or (subkey_type == "ELG-E" and 1024 <= int(subkey_length) <=3072) else False)
		checks_list.add_validation('name',True if ( len(name) > 4) and (name.find('<')==-1 or name.find('>')==-1 )  else False) 
		checks_list.add_validation('email',is_email_ok(email))  
		checks_list.add_validation('expire_date', is_expiration_date_ok(expiration_date))
		
		if checks_list.is_valid():
			key = gpg.gen_key_input(key_type=key_type,expire_date = expiration_date, passphrase = password, key_length=key_length, name_real = name,name_email = email,subkey_type = subkey_type,subkey_length=subkey_length)
			key = gpg.gen_key(key)
			if key:
				checks_list.add_result('status','generated_keys')
			else:
				checks_list.add_error('no_generated_keys')			
	except:
		checks_list.add_error('unknown_error')

	return checks_list
	