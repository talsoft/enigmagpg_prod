#  Copyright (C) 2013  Authors, TALSOFT S.R.L.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# 
# TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
# See the site of Enigma-gpg: http://www.enigmagpg.com

#from django.conf.urls.defaults import patterns, url
from django.conf.urls import patterns, url, include
from enigmaapi.apps.core import views 
from django.contrib import admin
from django.conf.urls import include
from django.views.generic.base import TemplateView


urlpatterns = patterns('enigmaapi.apps.core.views',
    url(r'^$', views.home, name='index'),
    url(r'^profile/', views.view_user_profile,name='userProfileView'),
    url(r'^user/invite/$',  views.view_form_invite_user,name='invite_user'),
    url(r'^user/invite/save/$',  views.activate_user,name='activate_invite_user'),
    url(r'^user/message/create/(\d+)/$',  views.view_form_message_create,name='message_create'),
    url(r'^user/message/decipher/$',  views.view_form_message_decipher_create,name='decipher_message_create'),
    url(r'^user/message/create/save$',  views.view_form_message_create,name='message_create_save'),
    url(r'^user/help/$', TemplateView.as_view(template_name="help.html"),name='help'), 
    url(r'^keymanager/password/request$',views.view_form_keypassword, name='request_keypassword'),
    url(r'^keymanager/password/complete', views.requestkey, name='requestkeypassword_complete'),
    url(r'^keymanager/password/forcerenew$', views.requestForceRenewKey, name='requestforcerenewkeys'),
    url(r'^keymanager/password/forcerenew/complete$', views.requestForceRenewKeyComplete, name='requestforcerenewkeyscomplete'),
    url(r'^accounts/profile/$', views.home, name='index'),
    url(r'^profile/save', views.save_profile,name='saveUserProfile'),
    #url(r'^search/keycontainer/$', views.ajax_keycontainer_search, name = 'keycontainer_search' ),
    #url(r'^search/keycontainer/getkey/(\d+)/$', views.ajax_get_keyPublic_userprofile, name = 'get_key_user' ),
)
