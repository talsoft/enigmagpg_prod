#  Copyright (C) 2013  Authors, TALSOFT S.R.L.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# 
# TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
# See the site of Enigma-gpg: http://www.enigmagpg.com

from tastypie.resources import ModelResource

from enigmaapi.apps.core.models import  UserProfile, KeyContainer
from tastypie.authorization import DjangoAuthorization
from enigmaapi.authentication import OAuth20Authentication
from tastypie.constants import ALL_WITH_RELATIONS
import base64




class KeyContainerResource(ModelResource):
    resource_name = 'key'
    class Meta:
        queryset = KeyContainer.objects.all()
        authorization =  DjangoAuthorization()
        authentication = OAuth20Authentication()
        list_allowed_methods = ['get', 'post']
        detail_allowed_methods = ['get', 'post']
        filtering = {
            'user': ALL_WITH_RELATIONS,
        }
    
    # Add public key of contacts
    def dehydrate(self, bundle):
        keycontainer = KeyContainer.objects.get(pk=bundle.obj.pk)
        bundle.data['username'] = keycontainer.userContact.user.username
        bundle.data['lastname'] = keycontainer.userContact.user.last_name
        bundle.data['firstname'] = keycontainer.userContact.user.first_name
        bundle.data['email'] = keycontainer.userContact.email
        bundle.data['company'] = keycontainer.userContact.company
        bundle.data['public_key'] = keycontainer.userContact.public_key
        if keycontainer.userContact.check_regenerateKeysDays(): 
            bundle.data['regenerateKeysUser'] = 'True'
        else:
            bundle.data['regenerateKeysUser'] = 'False'
        
        
        return bundle
      
    def apply_authorization_limits(self, request, object_list):
        return object_list.filter(user=request.user.pk)
    
    # Get Keycontainer of userprofile logged
    def get_object_list(self, request):
        this_users_posts = super(KeyContainerResource, self).get_object_list(request).filter(user=UserProfile.objects.get(user=request.user))
        return this_users_posts
        
class UserResource(ModelResource):
    resource_name = 'user'
    class Meta:
        queryset = UserProfile.objects.all()
        excludes = ['email', 'password', 'is_superuser','private_key']
        list_allowed_methods = ['get', 'post']
        detail_allowed_methods = ['get', 'post']
        authorization =  DjangoAuthorization()
        authentication = OAuth20Authentication()
    
    def dehydrate(self, bundle):
        userprofile = UserProfile.objects.get(pk=bundle.obj.pk)
        bundle.data['public_key'] = userprofile.public_key
        bundle.data['private_key'] = userprofile.encrypt(userprofile.uuid_field,  userprofile.private_key)
        bundle.data['username'] = userprofile.user.username
        bundle.data['lastname'] = userprofile.user.last_name
        bundle.data['firstname'] = userprofile.user.first_name
        bundle.data['email'] = userprofile.email
        bundle.data['company'] = userprofile.company   
        bundle.data['uuidenc'] = base64.b64encode(userprofile.uuid_field)  
        if userprofile.check_regenerateKeysDays(): 
            bundle.data['regenerateKeysUser'] = 'True'
        else:
            bundle.data['regenerateKeysUser'] = 'False'
        
        
        return bundle
        
    def apply_authorization_limits(self, request, object_list):
        return object_list.filter(user=request.user.pk)
    
    
        
    
    # get objects user logged
    def get_object_list(self, request):
        users = super(UserResource, self).get_object_list(request).filter(user=request.user)
        return users
    