#  Copyright (C) 2013  Authors, TALSOFT S.R.L.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# 
# TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
# See the site of Enigma-gpg: http://www.enigmagpg.com
# Django settings for enigmaapi project.
import os
import datetime

DEBUG = True

TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'enigmaapi',                  # Or path to database file if using sqlite3.
        'USER': 'user',                       # Not used with sqlite3.
        'PASSWORD': 'password',                   # Not used with sqlite3.
        'HOST': 'localhost',                  # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '3306',                       # Set to empty string for default. Not used with sqlite3.
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.4/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ''

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.normpath(os.path.join(os.path.dirname(__file__),'static/')),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Setting domain to allow register or "" for any
DOMAIN_ALLOW_REGISTER=""

# GENERATION KEYS GnuPG
# @param key_type Key's type to generate, The key type can be RSA or DSA.
GNUPG_KEY_TYPE = "RSA"
# @param key_length Key's length in bytes. Can be DSA(from 1024 to 3072) or RSA(from 1024 to 4096)  
GNUPG_KEY_LENGTH = 2048
# @param comment An additional comment
GNUPG_COMMENT = "Generate by EnigmaGPG - http://www.enigmagpg.com"
# @param subkey_type Subkey's type to generate, The subkey type can be RSA or ElGamal ELG-E.
GNUPG_SUBKEY_TYPE= "RSA"
# @param subkey_length The subkey's length in bytes. Can be RSA(from 1024 to 4096) or ElGamal(from 1024 to 3072)  
GNUPG_SUBKEY_LENGTH = 2048

# Make this unique, and don't share it with anybody.
SECRET_KEY = '(*%%1la)sql&amp;9sx6g@-a%t49v0j9(tmf#u6uag&amp;%=0_rrsdi54'

#EncryptedCharField - Encryption is handled by Keyczar.
#EncryptedCharField - Encryption is handled by Keyczar.
#Create the keyczar keys
#mkdir -p keys 
#keyczart create --location=/keys --purpose=crypt 
#keyczart addkey --location=/keys --status=primary 
ENCRYPTED_FIELD_KEYS_DIR = os.path.normpath(os.path.join(os.path.dirname(__file__),'keys/'))

# Send notification when an user is activate into the system
NOTIFY_ACTIVATION_MAILFROM = "mailfrom@example.com"
NOTIFY_ACTIVATION_MAILTO = "mailto@example.com"

# Setting security cookie
#SESSION_COOKIE_SECURE = True
#CSRF_COOKIE_SECURE = True
#SESSION_COOKIE_HTTPONLY = True
#CSRF_COOKIE_HTTPONLY = True

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'axes.middleware.FailedLoginMiddleware',
)

ROOT_URLCONF = 'enigmaapi.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'enigmaapi.wsgi.application'

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(os.path.dirname(__file__),'templates'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.formtools',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    'provider',
    'provider.oauth2',
    'tastypie',
    'registration',
    'django.contrib.humanize',
    'django_extensions',
    'passwords',
    'axes',
    #'allauth',
    #'allauth.account',
    #'allauth.socialaccount',
    #'allauth.socialaccount.providers.facebook',
    #'allauth.socialaccount.providers.google',
    #'allauth.socialaccount.providers.twitter',
    # Uncomment the next line to enable admin documentation:
    'enigmaapi.apps.core',
    # 'django.contrib.admindocs',
)

TASTYPIE_DEFAULT_FORMATS = ['json', 'xml']

# Set Expire token API
OAUTH_EXPIRE_DELTA=datetime.timedelta(days=1)

#ATTEMPS LOGIN
AXES_LOGIN_FAILURE_LIMIT= 6
AXES_LOCK_OUT_AT_FAILURE= True
AXES_USE_USER_AGENT= False
AXES_COOLOFF_TIME= 1
AXES_VERBOSE= False
AXES_LOCKOUT_TEMPLATE= os.path.join(os.path.dirname(__file__),'templates/locked.html')

# PASSWORD POLICY
PASSWORD_MIN_LENGTH = 8
PASSWORD_COMPLEXITY = { "UPPER":  1, "LOWER":  1, "DIGITS": 1 }

# ACTIVATION ACCOUNT
ACCOUNT_ACTIVATION_DAYS=2
EMAIL_HOST = 'mail.example.com'
EMAIL_PORT = 25
EMAIL_HOST_USER = "user@example.com"
EMAIL_HOST_PASSWORD = "password"
EMAIL_USE_TLS = False
DEFAULT_FROM_EMAIL = 'user@example.com'

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}


TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",                           
    "django.core.context_processors.request",
    "allauth.account.context_processors.account",
    "allauth.socialaccount.context_processors.socialaccount",
)

AUTHENTICATION_BACKENDS = (
    # Needed to login by username in Django admin, regardless of `allauth`
    "django.contrib.auth.backends.ModelBackend",
    # `allauth` specific authentication methods, such as login by e-mail
    "allauth.account.auth_backends.AuthenticationBackend",
)

SOCIALACCOUNT_PROVIDERS = \
    { 'facebook':
        { 'SCOPE': ['email', 'publish_stream'],
          'AUTH_PARAMS': { 'auth_type': 'reauthenticate' },
          'METHOD': 'oauth2' ,
          'LOCALE_FUNC': 'path.to.callable'} }
