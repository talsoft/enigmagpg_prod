LEGAL AGREEMENT:
Your use of EnigmaGPG is governed by the following conditions. Please read this information carefully before using EnigmaGPG . By using it you are agreeing to the following conditions:

* EnigmaGPG is released under the 'GNU General Public License, version 3'. See './LICENSE.md'.
* EnigmaGPG is supplied 'as-is'. The author assumes no liability for damages, direct or consequential, which may result from the use of EnigmaGPG Community.
* International users need to check for any import restrictions that your government may impose.

LICENSES:
- EnigmaGPG created by under the GNU General Public License, version 3.
- Openpgpjs.org: GNU Lesser General Public License (2.1) 


TRADEMARKS:

All other trademarks and trade names are properties of their respective owners. All Rights Reserved.
