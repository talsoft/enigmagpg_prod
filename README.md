ENIGMAGPG
================

EnigmaGPG allows you to encrypt messages. Ensuring the authenticity, privacy and confidentiality of messages for your Business.

Features:

- Allows communication messages private and confidential using public and private keys to encrypt and sign information.
- Does not store messages. Manage your public and private keys centrally.
- EnigmaGPG is a messenger platform simple, secure and user friendly for your business or friends.
- Available for mobile devices through iOS and Android application.
- Install, configure and manage a secure messaging platform for your business or group of friends.
- Simple, Secure & Free Software.

Steps:

- Register in Demo Live http://www.enigmagpg.com
- Login in.
- Invite contacts.

Write a message:

- Right-mouse click and go to Enigma GPG-> Cipher
- Select a contact and write a messages
- Copy and paste your cipher message in you gmail, facebook, o another web application.

Read a message:

- Right-mouse click and go to Enigma GPG-> Decipher
- Enter the Password and Decipher message.
- Paste message to Decipher EnigmaGPG.
 

Web Site: http://www.enigmagpg.com

Registration Demo Web Site: https://app.enigmagpg.com

Architecture
================

The system consists of 2 EnigmaGpg modules respectively:

- Daemon: This module is a web server that runs on the local network over HTTP/SSL. This takes care of exposing an API to Customer Module and interact with the system you have installed a browser addons. It's runs the user registration and user data. 
- Customer Module: This module are extensions of browsers, in this case, there are modules for Chrome and Firefox. This module displays a graphical user interface, a contextual menu appears with the right mouse button, and communicates with the daemon for API to perform all system funcionalides.


DEVELOPING IN
================

- Generate keys pair GPG in client side Chrome and Firefox
- Code refactoring server side


DAEMON 
======

Requeriments - Python Packages
==============================

Linux ubuntu: 

	sudo apt-get install python-gnupg
	sudo apt-get install ipython-notebook
	sudo apt-get install python-gnupginterface
	sudo apt-get install rng-tools
	sudo apt-get install python-cracklib
	sudo apt-get install python-jsonpickle
	sudo apt-get install python-simplejson
	sudo apt-get install python-tweepy

Django
=======

Linux ubuntu:

	sudo pip install Django==1.6.1
	sudo pip install django-tastypie==0.9.15
	sudo pip install defusedxml==0.4.1
	sudo pip install lxml==3.2.1
	sudo pip install django-extensions
	sudo pip install django-registration
	sudo pip install python-keyczar
	sudo pip install django-oauth2-provider
	sudo pip install django-passwords
	sudo pip install python-gnupg
	sudo pip install django-axes
	sudo pip install django-allauth


Database Connection
===================

	sudo apt-get install python-mysqldb
	sudo pip install mysql-python
	sudo apt-get install libmysqlclient-dev
	sudo apt-get install python-dev
	sudo apt-get install mysql-server



GENERATE KEYCZART for encrypted field DB
=========================================

In console:

- mkdir -p keys 
- keyczart create --location=/keys --purpose=crypt 
- keyczart addkey --location=/keys --status=primary 
- Change the directory string ENCRYPTED_FIELD_KEYS_DIR in setting.py 


Configure in setting.py
=====================

- the database connection in setting.py
- the DOMAIN_ALLOW_REGISTER for your domain
- the directory keyczart ENCRYPTED_FIELD_KEYS_DIR
- the mail server for activation and invitation accounts  

Run Server Django
=================

- Configure database connection in setting.py
- Sync DB Django: python manage.py syncdb
- Run in console: python manage.py runserver 8081
- Url browser: http://127.0.0.1:8081/

Customer Module
===============

Extensions for Chrome and Firefox
================================

- To create the client access for Oauth2 go to http://ipaddress:8081/admin/oauth2/ and login super user
- Create a client access with url api : http://ipaddress:8081/api/v1/
- In the Plugins folder can modify your plugins client access data and url of the server
- Install your plugins in your browser 
- Test login plugin with an user previously registered


Run App Django over Apache SSL
===============================

Add virtualhost with:

	<VirtualHost *:443>
		DocumentRoot /var/www/pathapp
		ServerName app.pathapp.com
		<Directory "/var/www/pathapp">
			allow from all
			Options -Indexes
		</Directory>
		Alias /static/ /var/www/pathapp/app/static/
		
		<Directory /var/www/pathapp/app/static>
			Order deny,allow
			Allow from all
		</Directory>
		
		WSGIDaemonProcess app.pathapp.com user=ubuntuuser group=ubuntuuser home=/home/ubuntuuser processes=2 threads=15 display-name=%{GROUP}
		WSGIProcessGroup app.pathapp.com
		WSGIScriptAlias / /var/www/app.pathapp.com/enigmagpg/enigmaapi/wsgi.py
		WSGIPassAuthorization On
		
	    SSLEngine on
	    SSLProtocol -ALL +SSLv3 +TLSv1
	    SSLCipherSuite ALL:!ADH:RC4+RSA:+HIGH:+MEDIUM:!LOW:!SSLv2:!EXPORT
	    SSLCertificateFile /etc/apache2/sslpathapp.com.cer
	    SSLCertificateKeyFile /etc/apache2/keypathapp.com.key
	    SSLCertificateChainFile /etc/apache2/intermediate
	    SSLCACertificateFile /etc/apache2/ca.pem
	</VirtualHost>


Develop with Eclipse
====================

- Eclipse: Version: 3.8.1
- Python: 2.7.4
- Django: 1.6.1
- Pydev: http://pydev.org/updates 2.8.2
- Git: http://download.eclipse.org/egit/updates
- PyDev for Eclipse
